<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-fw fa-tachometer-alt"></i>
        </div>
        <div class="sidebar-brand-text mx-3">{{ auth()->user()->name }}</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="{{ route('dashboard') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li> 

    <li class="nav-item">
        <a class="nav-link" href="{{ route('products.filter','product_name=&authers=&date=') }}">
            <i class="fas fa-fw fa-list"></i>
            <span>Products</span></a>
    </li> 

    <li class="nav-item">
        <a class="nav-link" href="{{ route('account.logout') }}">
            <i class="fas fa-fw fa-lock"></i>
            <span>Logout</span></a>
    </li> 
</ul>
<!-- End of Sidebar -->
