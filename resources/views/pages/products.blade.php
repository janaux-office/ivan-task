@extends('layout.admin')
@section('title','Products')
@section('content')
<div class="row">
    <div class="col-md-2">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('products.filter') }}">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Filter Products</label>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Product Name</label>
                        <input type="text" class="form-control" name="product_name" value="{{ $params['product_name'] }}">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">By Author</label>
                        <select class="form-control" name="authers">
                        <option value="">Select Author</option>
                        @foreach ($authors as $auth)
                            <option value="{{ $auth->id }}" @if($params['authers'] == $auth->id) selected @endif>{{ $auth->name }}</option>
                        @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Date</label>
                        <input type="date" class="form-control" name="date" value="{{ $params['date'] }}">
                    </div>

                    <hr>

                    <button type="submit" class="btn btn-block btn-info">Filter</button>
                    <a href="{{ route('products.filter','product_name=&authers=&date=') }}" class="btn btn-block btn-danger">Clear Filters</a>
                </form>
            </div>
        </div>
    </div>

    <div class="col-md-10">
    <div class="card">
        <div class="card-body">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Product Name</th>
                    <th>Author</th>
                    <th>Price</th>
                    <th>Status</th>
                    <th>Created On</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($products as $key => $product)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ ucwords($product->name) }}</td>
                        <td>{{ ucwords($product->author->name) }}</td>
                        <td>${{ $product->price }}.00</td>
                        <td>
                            @if($product->status == 1)
                                <span class="badge rounded-pill bg-primary text-light">Live</span>
                            @else
                                <span class="badge rounded-pill bg-danger text-light">Pending</span>
                            @endif
                        </td>
                        <td>{{ $product->created_at->format('d M,Y @ H:i A') }}</td>
                    </tr>
                    @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>
@endsection