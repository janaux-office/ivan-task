<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Products</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Open+Sans">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <style>
        body {
            background-color: #eee
        }

        .container {
            width: 900px
        }

        .card {
            background-color: #fff;
            border: none;
            border-radius: 10px;
            width: 190px;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)
        }

        .image-container {
            position: relative
        }

        .thumbnail-image {
            border-radius: 10px !important
        }

        .discount {
            background-color: red;
            padding-top: 1px;
            padding-bottom: 1px;
            padding-left: 4px;
            padding-right: 4px;
            font-size: 10px;
            border-radius: 6px;
            color: #fff
        }

        .wishlist {
            height: 25px;
            width: 25px;
            background-color: #eee;
            display: flex;
            justify-content: center;
            align-items: center;
            border-radius: 50%;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)
        }

        .first {
            position: absolute;
            width: 100%;
            padding: 9px
        }

        .dress-name {
            font-size: 13px;
            font-weight: bold;
            width: 75%
        }

        .new-price {
            font-size: 13px;
            font-weight: bold;
            color: red
        }

        .old-price {
            font-size: 8px;
            font-weight: bold;
            color: grey
        }

        .btn {
            width: 14px;
            height: 14px;
            border-radius: 50%;
            padding: 3px
        }

        .creme {
            background-color: #fff;
            border: 2px solid grey
        }

        .creme:hover {
            border: 3px solid grey
        }

        .creme:focus {
            background-color: grey
        }

        .red {
            background-color: #fff;
            border: 2px solid red
        }

        .red:hover {
            border: 3px solid red
        }

        .red:focus {
            background-color: red
        }

        .blue {
            background-color: #fff;
            border: 2px solid #40C4FF
        }

        .blue:hover {
            border: 3px solid #40C4FF
        }

        .blue:focus {
            background-color: #40C4FF
        }

        .darkblue {
            background-color: #fff;
            border: 2px solid #01579B
        }

        .darkblue:hover {
            border: 3px solid #01579B
        }

        .darkblue:focus {
            background-color: #01579B
        }

        .yellow {
            background-color: #fff;
            border: 2px solid #FFCA28
        }

        .yellow:hover {
            border-radius: 3px solid #FFCA28
        }

        .yellow:focus {
            background-color: #FFCA28
        }

        .item-size {
            width: 15px;
            height: 15px;
            border-radius: 50%;
            background: #fff;
            border: 1px solid grey;
            color: grey;
            font-size: 10px;
            text-align: center;
            align-items: center;
            display: flex;
            justify-content: center
        }

        .rating-star {
            font-size: 10px !important
        }

        .rating-number {
            font-size: 10px;
            color: grey
        }

        .buy {
            font-size: 12px;
            color: purple;
            font-weight: 500
        }

        .voutchers {
            background-color: #fff;
            border: none;
            border-radius: 10px;
            width: 190px;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            overflow: hidden
        }

        .voutcher-divider {
            display: flex
        }

        .voutcher-left {
            width: 60%
        }

        .voutcher-name {
            color: grey;
            font-size: 9px;
            font-weight: 500
        }

        .voutcher-code {
            color: red;
            font-size: 11px;
            font-weight: bold
        }

        .voutcher-right {
            width: 40%;
            background-color: purple;
            color: #fff
        }

        .discount-percent {
            font-size: 12px;
            font-weight: bold;
            position: relative;
            top: 5px
        }

        .off {
            font-size: 14px;
            position: relative;
            bottom: 5px
        }
    </style>
    <script>
        $(document).ready(function () {
            $(".wish-icon i").click(function () {
                $(this).toggleClass("fa-heart fa-heart-o");
            });
        });
    </script>
</head>

<body>

    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
        <h5 class="my-0 mr-md-auto font-weight-normal">Ivan Products</h5>
        <nav class="my-2 my-md-0 mr-md-3">
            <a class="p-2 text-dark" href="{{ route('account.signup') }}">Register</a>
            <a class="p-2 text-dark" href="{{ route('account.login') }}">Login</a>
        </nav>
    </div>

    {{-- <div class="container mt-5">
        <div class="row">
            @foreach ($products as $product)
            <div class="col-md-3">
                <div class="card">
                    <div class="image-container">
                        <img src="https://i.imgur.com/8JIWpnw.jpg" class="img-fluid rounded thumbnail-image">
                    </div>
                    <div class="product-detail-container p-2">
                        <div class="d-flex justify-content-between align-items-center">
                            <h5 class="dress-name">White traditional long dress</h5>
                            <div class="d-flex flex-column mb-2">
                                <span class="new-price">$3.99</span>
                                <small class="old-price text-right">$5.99</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div> --}}


</body>

</html>