<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Authentication Routes
|--------------------------------------------------------------------------
*/

Route::get('signin', 'AuthController@signin')->name('account.login');
Route::get('signup/{type?}'   , 'AuthController@signup')->name('account.signup');
Route::post('register/{type?}', 'AuthController@register')->name('account.register');
Route::post('auth/login', 'AuthController@authenticate')->name('account.auth');
Route::get('signout','AuthController@signout')->name('account.logout');
