<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| App Routes
|--------------------------------------------------------------------------
*/

Route::group(['middleware' => 'auth'], function()
{
    Route::get('/', 'AccountController@index');
    Route::get('dashboard', 'AccountController@dashboard')->name('dashboard');
    Route::get('products/filter', 'AccountController@filter')->name('products.filter');
});
