<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Models\Products;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AccountController extends Controller
{
    //
    public function index(){
        return redirect()->route('dashboard');
    }

    //
    public function dashboard(){
        $profile = auth()->user();
        return view('pages.dashboard',compact('profile'));
    }

    //
    public function products(){
        $products = Products::all();
        $authors  = User::all();
        return view('pages.products',compact('products','authors'));
    }

    public function filter(Request $request)
    {
        $authors  = User::all();
        $params   = $request->all();

        $q = Products::query();

        if(!empty($request->product_name)){
            $q->searchByName($request->product_name);
        }
        if(!empty($request->authers)){
            $q->searchByAuthor($request->authers);
        }
        if(!empty($request->date)){
            $q->searchByDate($request->date);
        }
        $products = $q->orderBy('id')->get();
        return view('pages.products',compact('products','authors','params'));
    }

    public function updateProfile(Request $request){
        //
        $validator = Validator::make($request->all(),[
            "fname"    => 'required',
            "lname"    => 'required',
            "mobile"   => 'required|min:10|max:10',
            "address"  => 'required',
            "country"  => 'required',
            "state"    => 'required',
            "city"     => 'required',
            "pincode"  => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
        }
        $profile = Profile::where('user_id',auth()->user()->id)->first();
        if($request->hasFile('avatar')){
            $fileName = time().'.'.$request->avatar->extension();
            $request['picture'] = $fileName;
            $request->avatar->move(public_path('uploads'), $fileName);
        	//
        	if($profile->picture != null || !empty($profile->picture)){
            	unlink(public_path('uploads/'.$profile->picture));
            }
        }
        $profile->update($request->all());
        return redirect()->back()->with('status','Profile updated');
    }

}
