<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    //
    public function index()
    {
        if(Auth::check()){
            return redirect()->intended('account/dashboard');
        }else{
            return redirect()->intended('account/signin/');
        }
    }

    /**
     * Open User signin form
    */
    public function signin(){
        if(Auth::check()){
            return redirect()->intended('account/dashboard');
        }else{
            return view('auth.login');
        }
    }

    /**
     * Open User signin form
    */
    public function admin_signin(){
        return view('auth.admin_login');
    }

    public function signup($type = 'customer')
    {
        return view('auth.register')->with('type',$type);
    }

    public function register(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'email'   => 'required|unique:users',
            'name'    => 'required',
            'password' =>'required|min:6',
            'confirm_password'=>'required|same:password|min:6',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
        }

        $user = new User;
        $user->name     = $request->input('name');
        $user->email    = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->save();
        //
        Auth::loginUsingId($user->id);
        return redirect()->route('dashboard');
    }

    /**
    * Authenticate user and Logged in
    */
    public function authenticate(Request $request){
        //
        $validator = Validator::make($request->all(),[
            'email'    => 'required',
            'password' => 'required|min:6',
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        if (Auth::attempt($request->only('email', 'password'))) {
            // Authentication passed...
            $redirectTo = 'account/dashboard';
            return redirect()->intended($redirectTo);
        }
        else {
            // Authentication failed...
            return redirect()->back()->with('error', 'Invalid Username or Password');
        }

    }

    /**
    * Signout current signed in user
    */
    public function signout(Request $request){
        //
        auth()->logout();
        return redirect()->intended('user/signin');
    }
}
