<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;

    /**
     * Get the author associated with the Products
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function author()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function scopeSearchByName($query, $name)
    {
        $query->where('name',$name);
    }

    public function scopeSearchByAuthor($query, $uid)
    {
        $query->where('user_id',$uid);
    }

    public function scopeSearchByDate($query, $date)
    {
        $query->whereDate('created_at',$date);
    }

}
